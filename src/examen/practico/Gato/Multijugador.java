/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen.practico.Gato;

/**
 *
 * @author oscar
 */
public class Multijugador extends javax.swing.JFrame {
    private boolean jugador;
    private String[][] movi;
    
    
    /**
     * Creates new form Multijugador
     */
    public Multijugador() {
        
        jugador = true; //se inicializa en el jujador 1 que son X
        movi = new String[3][3];
        llenarMatriz();
       
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        Bton1 = new javax.swing.JButton();
        Bton3 = new javax.swing.JButton();
        Bton4 = new javax.swing.JButton();
        Bton5 = new javax.swing.JButton();
        Bton6 = new javax.swing.JButton();
        Bton7 = new javax.swing.JButton();
        Bton8 = new javax.swing.JButton();
        Bton9 = new javax.swing.JButton();
        Bton2 = new javax.swing.JButton();
        Bsalir2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(153, 153, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel1.setText("Modo 2 Jugadores");

        Bton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Bton1ActionPerformed(evt);
            }
        });

        Bton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Bton3ActionPerformed(evt);
            }
        });

        Bton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Bton4ActionPerformed(evt);
            }
        });

        Bton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Bton5ActionPerformed(evt);
            }
        });

        Bton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Bton6ActionPerformed(evt);
            }
        });

        Bton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Bton7ActionPerformed(evt);
            }
        });

        Bton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Bton8ActionPerformed(evt);
            }
        });

        Bton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Bton9ActionPerformed(evt);
            }
        });

        Bton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Bton2ActionPerformed(evt);
            }
        });

        Bsalir2.setText("Salir");
        Bsalir2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Bsalir2ActionPerformed(evt);
            }
        });

        jButton1.setText("Reinciar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel1))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(Bsalir2, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(Bton7, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(Bton8, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Bton9, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(Bton4, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(Bton5, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, Short.MAX_VALUE)
                            .addComponent(Bton6, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(Bton1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(Bton2, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Bton3, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(50, 50, 50))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Bton1, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Bton3, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Bton2, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Bton4, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Bton5, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Bton6, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Bton7, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Bton9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(Bton8, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(Bsalir2))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 2, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Bsalir2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Bsalir2ActionPerformed

   Menu menu= new Menu();
            menu.setVisible(true);
            this.dispose();

    }//GEN-LAST:event_Bsalir2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
  llenarMatriz();
        Bton1.setText("");
        Bton2.setText("");
        Bton3.setText("");
        Bton4.setText("");
        Bton5.setText("");
        Bton6.setText("");
        Bton7.setText("");
        Bton8.setText("");
        Bton9.setText("");
        jugador = true;

    }//GEN-LAST:event_jButton1ActionPerformed

    private void Bton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Bton1ActionPerformed
  if(jugador && movi[0][0].equals("i"))
       {
           Bton1.setText("X");
           setMovimiento(0,0,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 1 Gana");
       }
       else if(!jugador && movi[0][0].equals("i"))
       {
           Bton1.setText("O");
           setMovimiento(0,0,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 2 Gana");
       }
    }//GEN-LAST:event_Bton1ActionPerformed

    private void Bton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Bton2ActionPerformed
            if(jugador && movi[0][1].equals("i")){
                                     
           Bton2.setText("X");
           setMovimiento(0,1,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 1 Gana");
       }
       else if(!jugador && movi[0][1].equals("i"))
       {
           Bton2.setText("O");
           setMovimiento(0,1,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 2 Gana");
       }



    }//GEN-LAST:event_Bton2ActionPerformed

    private void Bton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Bton3ActionPerformed
       if(jugador && movi[0][2].equals("i"))
       {
           Bton3.setText("X");
           setMovimiento(0,2,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 1 Gana");
       }
       else if(!jugador && movi[0][2].equals("i"))
       {
           Bton3.setText("O");
           setMovimiento(0,2,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 2 Gana");
       }
    }//GEN-LAST:event_Bton3ActionPerformed

    private void Bton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Bton4ActionPerformed
       if(jugador && movi[1][0].equals("i"))
       {
           Bton4.setText("X");
           setMovimiento(1,0,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 1 Gana");
       }
       else if(!jugador && movi[1][0].equals("i"))
       {
           Bton4.setText("O");
           setMovimiento(1,0,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 2 Gana");
       }

    }//GEN-LAST:event_Bton4ActionPerformed

    private void Bton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Bton5ActionPerformed
       if(jugador && movi[1][1].equals("i"))
       {
           Bton5.setText("X");
           setMovimiento(1,1,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 1 Gana");
       }
       else if(!jugador && movi[1][1].equals("i"))
       {
           Bton5.setText("O");
           setMovimiento(1,1,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 2 Gana");
       }


    }//GEN-LAST:event_Bton5ActionPerformed

    private void Bton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Bton6ActionPerformed
       if(jugador && movi[1][2].equals("i"))
       {
           Bton6.setText("X");
           setMovimiento(1,2,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 1 Gana");
       }
       else if(!jugador && movi[1][2].equals("i"))
       {
           Bton6.setText("O");
           setMovimiento(1,2,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 2 Gana");
       }



    }//GEN-LAST:event_Bton6ActionPerformed

    private void Bton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Bton7ActionPerformed
       if(jugador && movi[2][0].equals("i"))
       {
           Bton7.setText("X");
           setMovimiento(2,0,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 1 Gana");
       }
       else if(!jugador && movi[2][0].equals("i"))
       {
           Bton7.setText("O");
           setMovimiento(2,0,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 2 Gana");
       }

    }//GEN-LAST:event_Bton7ActionPerformed

    private void Bton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Bton8ActionPerformed
       if(jugador && movi[2][1].equals("i"))
       {
           Bton8.setText("X");
           setMovimiento(2,1,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 1 Gana");
       }
       else if(!jugador && movi[2][1].equals("i"))
       {
           Bton8.setText("O");
           setMovimiento(2,1,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 2 Gana");
       }

    }//GEN-LAST:event_Bton8ActionPerformed

    private void Bton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Bton9ActionPerformed
         if(jugador && movi[2][2].equals("i"))
       {
           Bton9.setText("X");
           setMovimiento(2,2,"X");
           jugador = false;
           imprimirMatriz();
           if(ganador1())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 1 Gana");
       }
       else if(!jugador && movi[2][2].equals("i"))
       {
           Bton9.setText("O");
           setMovimiento(2,2,"O");
           jugador = true;
           imprimirMatriz();
           if(ganador2())
               javax.swing.JOptionPane.showMessageDialog(null, "El Jugador 2 Gana");
       }
    }//GEN-LAST:event_Bton9ActionPerformed

    
    
    
        
    public boolean ganador1()
    {
        if( ((movi[0][0].equals("X")) && (movi[0][1].equals("X")) && (movi[0][2].equals("X")))
                || ((movi[1][0].equals("X")) && (movi[1][1].equals("X")) && (movi[1][2].equals("X")))
                || ((movi[2][0].equals("X")) && (movi[2][1].equals("X")) && (movi[2][2].equals("X")))
                || ((movi[0][0].equals("X")) && (movi[1][0].equals("X")) && (movi[2][0].equals("X")))
                || ((movi[0][1].equals("X")) && (movi[1][1].equals("X")) && (movi[2][1].equals("X")))
                || ((movi[0][2].equals("X")) && (movi[1][2].equals("X")) && (movi[2][2].equals("X")))
                || ((movi[0][0].equals("X")) && (movi[1][1].equals("X")) && (movi[2][2].equals("X")))
                || ((movi[0][2].equals("X")) && (movi[1][1].equals("X")) && (movi[2][0].equals("X"))))
                return true;
                
         else
            return false;
    }
    
    public boolean ganador2()
    {
        if( ((movi[0][0].equals("O")) && (movi[0][1].equals("O")) && (movi[0][2].equals("O")))
                || ((movi[1][0].equals("O")) && (movi[1][1].equals("O")) && (movi[1][2].equals("O")))
                || ((movi[2][0].equals("O")) && (movi[2][1].equals("O")) && (movi[2][2].equals("O")))
                || ((movi[0][0].equals("O")) && (movi[1][0].equals("O")) && (movi[2][0].equals("O")))
                || ((movi[0][1].equals("O")) && (movi[1][1].equals("O")) && (movi[2][1].equals("O")))
                || ((movi[0][2].equals("O")) && (movi[1][2].equals("O")) && (movi[2][2].equals("O")))
                || ((movi[0][0].equals("O")) && (movi[1][1].equals("O")) && (movi[2][2].equals("O")))
                || ((movi[0][2].equals("O")) && (movi[1][1].equals("O")) && (movi[2][0].equals("O"))))
                return true;
                
         else
            return false;
    }
    
    public void setMovimiento(int i, int j, String mo)
    {
        movi[i][j] = mo;
    }
    public void llenarMatriz()
    {
        for(int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 3; j++)
            {
                movi[i][j] = "i";
            }
        }
    }
    
    public void imprimirMatriz()
    {
        for(int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 3; j++)
            {
                System.out.print(movi[i][j]);
            }
            System.out.print("\n");
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Multijugador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Multijugador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Multijugador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Multijugador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Multijugador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Bsalir2;
    private javax.swing.JButton Bton1;
    private javax.swing.JButton Bton2;
    private javax.swing.JButton Bton3;
    private javax.swing.JButton Bton4;
    private javax.swing.JButton Bton5;
    private javax.swing.JButton Bton6;
    private javax.swing.JButton Bton7;
    private javax.swing.JButton Bton8;
    private javax.swing.JButton Bton9;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
